#!/bin/sh
# this will probably work with docker just as well as podman

podman run --rm \
  -v ..:/usr/src/build/ \
  -v ./buster-target:/usr/src/build/webmetro/target \
  -w /usr/src/build/webmetro \
  rust:1-buster cargo build --release
